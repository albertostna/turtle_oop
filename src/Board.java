import greenfoot.*;

/**
 * The Board class defines a board of 11 cells on the X axis by 5 cells 
 * on the Y axis, where each cell is drawn using a 60x60 size image 
 * built from the "sand.jpg" file. When a scenario is executed, Greenfoot 
 * executes the act() method of all the actors repeatedly, that is, the 
 * execution of a Greenfoot scenario is a sequence of cycles and in each 
 * cycle the act () method of all the objects it contains is executed 
 * scenario.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
public class Board extends World {
	private Turtle turtle;

    public Board() {  
        super(11, 5, 60); 
        turtle= new Turtle();
        addObject(turtle, 5, 1);
    }

    public Turtle getTurtle() {
    	return turtle;
    }
    
}
