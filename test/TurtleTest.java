import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
* Test of Turtle class. 
* 
* @author (Francisco Guerra) 
* @version (Version 1)
*/
@RunWith(GreenfootRunner.class)
public class TurtleTest {

    Board  board;	

    @Before
    public void setUp() throws Exception {
        board  = new Board();
    }

    @Test
    public void testImage() throws Exception {
        // Given
        Turtle turtle = new Turtle();

        // When
        GreenfootImage image = turtle.getImage();
        
        // Then
		assertEquals("turtle.png", image.getImageFileName());
    }

    @Test
    public void testImageWidth() throws Exception {
        // Given
        Turtle turtle = new Turtle();

        // When
        GreenfootImage image = turtle.getImage();
        
        // Then
		assertEquals(50, image.getWidth());
    }

    @Test
    public void testImageHeight() throws Exception {
        // Given
        Turtle turtle = new Turtle();

        // When
        GreenfootImage image = turtle.getImage();
        
        // Then
		assertEquals(40, image.getHeight());
    }

    @Test
    public void testTurtleAct_X() throws Exception {
        // Given
    	Turtle turtle = board.getTurtle();
        // When
    	turtle.act();
        // Then
    	assertEquals(6, turtle.getX());
    }

    @Test
    public void testTurtleAct_Y() throws Exception {
        // Given
    	Turtle turtle = board.getTurtle();
        // When
    	turtle.act();
        // Then
    	assertEquals(2, turtle.getY());
    }

    @Test
    public void testSetAngle180() throws Exception {
        // Given
    	Turtle turtle = board.getTurtle();
        // When
    	turtle.setRotation(180);
    	turtle.act();
   
        // Then
    	assertEquals(4, turtle.getX());
    }
    
    @Test
    public void testSetAngle270() throws Exception {
        // Given
    	Turtle turtle = board.getTurtle();
        // When
    	turtle.setAngle(270);
    	turtle.act();
        // Then
		assertEquals(0, turtle.getY());
    }
    
    @Test
    public void testSetDistance5Angle0() throws Exception {
    	  // Given
    	Turtle turtle = board.getTurtle();
        // When
    	turtle.setDistance(5);
    	turtle.setAngle(0);
    	turtle.act();
        // Then
		assertEquals(10, turtle.getX());
    }

    @Test
    public void testSetDistance3Angle90() throws Exception {
        // Given
    	Turtle turtle = board.getTurtle();
        // When
    	turtle.setDistance(3);
    	turtle.setAngle(90);
    	turtle.act();
        // Then
		assertEquals(4, turtle.getY());
    }
    
}
