import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.junitUtils.runner.GreenfootRunner;

/**
* Test of Board class. 
* 
* @author (Francisco Guerra) 
* @version (Version 1)
*/
@RunWith(GreenfootRunner.class)
public class BoardTest {

    Board  board;	

    @Before
    public void setUp() throws Exception {
        board  = new Board();
    }

    @Test
    public void testGetTurtle_X() throws Exception {
        // Given
    	Board board = new Board();	
    	Turtle turtle = board.getTurtle();
        // When
    	int x= turtle.getX();
        // Then
    	assertEquals(5,x);
    }

    @Test
    public void testGetTurtle_Y() throws Exception {
        // Given
    	Turtle turtle = board.getTurtle();
        // When
    	int y = turtle.getY();
        // Then
    	assertEquals(1,y);
    }

}
